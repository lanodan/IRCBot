A simple IRCBot for only one channel.
It works on all IRC servers (designed with rfc1459).

#Features

Parse one channel for links(ftp, http, https), commands, and easter eggs as well !
links are logged in IRCBot.log using csv in format ``url; mimeType; title``

#Commands
syntax: ``[optional] <required>``

``!hi [text]``               Welcome users. (the bot doesn't welcome users automatically yet)

``!say <text>``              say some text

``!action <text>``           do some actions(same as the /me)

``!source``                  Give a link to the sourcode

``!stop in the name of sey`` Stop the bot with a nice reference to SYRSA 

``!nsidentify``              Identify to NickServ using ``NS`` command

``!msgidentify``             Identify to NickServ using ``MSG NickServ``

#Configuration
(``conf.py`` check ``conf.py.example`` for example)

``nick``        nickname

``passwd``      password

``server``      Server IP/address

``port``        Server Port

``channel``     Channel, you can put only one channel for now

``quitMsg``     The message when the bot quits the server

``welcomeMsg``  The message when the bot arrives in the channel

``source``      The sourcecode of the bot

``NickServ``	The nick of the Nick identifier service (used by !msgidentify)

#Know Bugs/Exploits/…
* Command in the middle of the message by prepending a colon.
